# hypem-dl

## Installation

`./install.sh`

## Usage

`python hypem.py`

## Dependencies

python

python3

python-pip3


flyingscrub/scdl

`pip3 install scdl`


iheanyi/bandcamp-dl

##### OSX

`brew install bandcamp-downloader`

##### linux

`pip3 install bandcamp-downloader`
