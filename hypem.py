#!/usr/local/bin/python
# Author: Edward Vetter-Drake
# Version: 0.5
# 

from bs4 import BeautifulSoup
import os
import re
import requests

def main():
    """
    ask the user for their hypem.com username and download their favorited tracks
    """
    
    hypem_username = raw_input('input Hypem username > ')
    SC_links = search_SC_videos(hypem_username)
    for link in SC_links:
        sc_dl(extract_SC_track(link))

    BC_links = search_BC_videos(hypem_username)
    for link in BC_links:
        bc_dl(extract_BC_videos(link))



def search_BC_videos(query):
    """
    scrape the users favorites page for any bandcamp links for download

    :returns: list of redirect links to bandcamp.com
    """

    response = makeRequest('http://www.hypem.com/' + query, {})
    soup = BeautifulSoup(response.content, 'html.parser')
    found = BeautifulSoup(str(soup.find_all('div', 'meta')), 'html.parser')
    pattern = re.compile(r'/go/bc')
    meta = found.find_all('a', href=pattern)
    return [(x.get('href')) for x in meta]

def extract_SC_videos(html):
    """
    scrape hypem.com/<user> for the favorite tracks that are available on soundcloud.
    
    :returns: a list of links to the soundcloud page of each song
    :rtype: list
    """

    soup = BeautifulSoup(html, 'html.parser')
    found = BeautifulSoup(str(soup.find_all('div', 'section-player')), 'html.parser')
    pattern = re.compile(r'/go/sc')
    meta = found.find_all('a',  href=pattern)
    return [(x.get('href')) for x in meta]

def search_SC_videos(query):
    """
    Searches for tracks from hypem favorites given a query
    """
    response = makeRequest('http://www.hypem.com/' + query, {})
    return extract_SC_videos(response.content)


def makeRequest(url, hdr):
    """
    generic requeter funciton

    :returns: req object
    """
    http_proxy  = os.environ.get("HTTP_PROXY")
    https_proxy = os.environ.get("HTTPS_PROXY")
    ftp_proxy   = os.environ.get("FTP_PROXY")

    proxyDict = { 
        "http"  : http_proxy,
        "https" : https_proxy,
        "ftp"   : ftp_proxy
        }

    req = requests.get(url, headers=hdr, proxies=proxyDict)
    return req

def extract_BC_videos(bc_query):
    """
    recieve the redirect link to the direct listen page for bandcamp songs 

    :returns: bc_link for bc_dl
    """
    response = requests.head('http://www.hypem.com/' + bc_query, allow_redirects=True)
    return response.url


def extract_SC_track(sc_query):
    """
    recieve the redirect link to the direct listen page for soundcloud soungs
    :returns: sc_link for sc_dl
    """
    response = requests.head('http://www.hypem.com/' + sc_query, allow_redirects=True)
    return response.url

def bc_dl(bc_link):
    """
    download a bandcamp track from the direct link to the BC page
    """

    command_tokens = [
            'bandcamp-dl',
            '--no-art',
            '--template=%{title}_-_%{artist}',
            str(bc_link)
            ]

    command = ' '.join(command_tokens)

    os.system(command)

def sc_dl(sc_link):
    """
    download a soundcloud track from the direct link to the SC page
    """

    command_tokens = [
            'scdl',
            '-l',
            sc_link,
            ]

    command = ' '.join(command_tokens)
    
    os.system(command)

if __name__ == "__main__":
    main()
