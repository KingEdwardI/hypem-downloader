if [[ $OSTYPE == "darwin"* ]]; then
  brew install python python3 bandcamp-dl
elif [[ $OSTYPE == "linux-gnu" ]]; then
  sudo apt install python-pip python3-pip
  pip3 install bandcamp-dl
else
  echo "Operating system type not supported"
fi

pip3 install scdl
pip3 install bs4
pip install bs4

echo "Finished installing hypem-dl"
